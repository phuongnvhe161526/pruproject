using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallMove : MonoBehaviour
{
    public float moveSpeed;
    public float minY;
    public float maxY;
    private float oldPositionX;
    private GameObject obj;
    // Start is called before the first frame update
    void Start()
    {
        obj = gameObject;
        oldPositionX = 10;
        moveSpeed = 3;
        minY = 2;
        maxY = 5;
    }

    // Update is called once per frame
    void Update()
    {
        obj.transform.Translate(new Vector3(-1 * Time.deltaTime * moveSpeed, 0, 0));
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //obj.transform.position = new Vector3(oldPositionX, Random.Range(minY, maxY), 0);
        obj.transform.position = new Vector3(oldPositionX, Random.Range(-4, -2), 0);
    }
}
