using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;

public class RobotMove : MonoBehaviour
{
    [SerializeField] float speed = 5;
    [SerializeField] LayerMask groundLayer;
    private Rigidbody2D body;
    private Animator anim;
    private BoxCollider2D boxCollider;

    private void Awake()
    {
        body = gameObject.GetComponent<Rigidbody2D>();
        anim = gameObject.GetComponent<Animator>();
        boxCollider = gameObject.GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        body.velocity = new Vector2(horizontalInput * speed, body.velocity.y);
        if (horizontalInput > 0.01f)
        {
            transform.localScale = Vector3.one * 0.4f;
        }
        else if (horizontalInput < -0.01f)
        {
            transform.localScale = new Vector3(-1, 1, 1) * 0.4f;
        }

        if (verticalInput>0.01f && isGrounded())
        {
            Jump();
        } else if (verticalInput < -0.01f)
        {
            Slide();
        }
            

        //set animator
        anim.SetBool("run", horizontalInput != 0);
        anim.SetBool("isGround", isGrounded());
    }

    private void Jump()
    {
        body.velocity = new Vector2(body.velocity.x, 5);
        anim.SetTrigger("jump");
    }
    private void Slide()
    {
        //body.velocity = new Vector2(body.velocity.x, speed);
        anim.SetTrigger("slide");
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {

    }
    private bool isGrounded()
    {
        RaycastHit2D raycastHit = Physics2D.BoxCast(boxCollider.bounds.center,boxCollider.bounds.size,0,Vector2.down,0.1f,groundLayer);
        return raycastHit.collider!=null;
    }
}
