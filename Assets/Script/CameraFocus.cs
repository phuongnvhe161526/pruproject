using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFocus : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] GameObject Square;
    [SerializeField] GameObject land;
    [SerializeField] Vector3 offset = new Vector3(0, 0, 0);
    Vector3 oldCamposition, oldSquarePosition;
    void Start()
    {
        oldCamposition = transform.position;
        oldSquarePosition = Square.transform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 camPosition = Square.transform.position;
        camPosition.z = transform.position.z;
        transform.position = camPosition + offset;
    }
}
